import json
import boto3
from urllib3 import PoolManager, response
from urllib3.exceptions import ResponseError
import os
from botocore.exceptions import ClientError
from AppConfigHandler import get_appconfig

db = boto3.resource("dynamodb")

http = PoolManager()


class ResponseStatusError(ResponseError):

    def __init__(self, *args):
        if args:
            self.message = args[0]
        else:
            self.message = None



def start_pf(event, context):

    config = get_appconfig(
        app=os.environ.get("APPCONFIG_APPLICATION"),
        env=os.environ.get("APPCONFIG_ENVIRONMENT"),
        config=os.environ.get("APPCONFIG_CONFIGURATION")
    )
    
    try:
        response = http.request('GET', config.get("cadastra_pf_url"))
        print(response.status)
        print(response.data)

        if response.status >= 400 and response.status <= 599:
            raise ResponseStatusError("teste")
        
    except ResponseStatusError as err:
        raise err

    print(event)
    return {
        "pessoa": event
    }

