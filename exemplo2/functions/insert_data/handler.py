import json
import boto3
import os
from botocore.exceptions import ClientError

db = boto3.resource("dynamodb")
client = boto3.client('stepfunctions')


def insert_data(event, context):
    body = event.get("body")
    empresa = body.get("empresa")
    pessoas = body.get("pessoas")

    table_name = os.environ.get("TABLE_NAME")
    table = db.Table(table_name)

    try:
        table.put_item(
            Item={
                "pk": empresa["cnpj"],
                "sk": empresa["cnpj"],
                "nome": empresa["nome"],
                "endereco": empresa["endereco"]
            }
        )
        for pessoa in pessoas:
            table.put_item(
                Item={
                    "pk": empresa["cnpj"],
                    "sk": pessoa["cpf"],
                    "nome": pessoa["nome"],
                    "endereco": pessoa["endereco"]
                }
            )
    except Exception as e:
        print(e)
        return {
            "statusCode": 500,
            "body": {
                "code": e.response["Error"]["Code"],
                "message": e.response["Error"]["Message"]
            }
        }
    else:

        response = client.start_execution(
            # name=empresa["cnpj"],
            stateMachineArn='arn:aws:states:us-east-1:549672552044:stateMachine:dev-exemplo2-step-function',
            input=json.dumps({
                "body": body
            })
        )

        return {
            "statusCode": 200,
            "body": "Item inserido com sucesso"
        }


