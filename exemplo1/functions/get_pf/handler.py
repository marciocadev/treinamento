import json
import boto3
import os
from botocore.exceptions import ClientError
db = boto3.resource("dynamodb")

import urllib3

def get_pf(event, context):
    print(event)

    http = urllib3.PoolManager()
    r = http.request('GET', 'http://httpbin.org/status/200')
    print(r.status)
    print(r.data)

    try:
        cpf = event.get('path')['cpf']    
    except:         
        cpf = event.get('pathParameters')['cpf']

    try:
        table_name = os.environ.get("TABLE_NAME")
        table = db.Table(table_name)
        response = table.get_item(
            Key={
                "cpf": int(cpf)
            }
        )
        print(response["Item"]["nome"])
        return {
            "statusCode": 200,
            "body": "Item recuperado com sucesso: " + response["Item"]["nome"]
        }
    except ClientError as e:
        print(e.response['Error']['Message'])