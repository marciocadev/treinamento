import json
import boto3
import os
from botocore.exceptions import ClientError

db = boto3.resource("dynamodb")

def insert_pf(event, context):
    body = event.get("body")
    cpf = body.get("cpf")
    nome = body.get("nome")

    if cpf is None:
        return {
            "statusCode": 422,
            "body": "Invalid data"
        }

    table_name = os.environ.get("TABLE_NAME")
    table = db.Table(table_name)

    try:
        table.put_item(
            Item={
                "cpf": cpf,
                "nome": nome
            }
        )
    except Exception as e:
        return {
            "statusCode": 500,
            "body": {
                "code": e.response["Error"]["Code"],
                "message": e.response["Error"]["Message"]
            }
        }
    else:
        return {
            "statusCode": 200,
            "body": "Item inserido com sucesso"
        }
